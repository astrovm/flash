# Flash Collection

A curated archive of Flash games

## Features

- Flash emulation by Ruffle
- Play offline, no internet connection required
- FPS optimized per game
- Fast CDN
- Flawless screen adaptation
- No requests to external resources
- Automatic sitelock bypass

## Local test

```bash
python -m http.server
```

## Regenerate service worker

```bash
workbox generateSW workbox-config.js
```
